package com.example.controller;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Users;
import com.example.repository.UserRepository;

@RestController
public class AdminController {

	UserRepository userRepository;
	PasswordEncoder passwordEncoder;

	@Autowired
	public AdminController(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@GetMapping(value = "/admin/private")
	public String forAdmin() {
		Collection<? extends GrantedAuthority> authorityList = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		String havingAuthority = authorityList.stream().map(GrantedAuthority :: getAuthority)
				.collect(Collectors.joining(","));
		return "Hello Admin having authority = " + havingAuthority;
	}

	@PostMapping(value = "/admin/add")
	public String addUserByAdmin(@RequestBody Users user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		return "User Added successfully !";
	}

	@GetMapping(value = "/user/public")
	public String forUser() {
		Collection<? extends GrantedAuthority> authorityList = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		String havingAuthority = authorityList.stream().map(GrantedAuthority :: getAuthority)
				.collect(Collectors.joining(","));
		return "Hello User having authority = " + havingAuthority;
	}

	@GetMapping(value = "/all")
	public String all() {
		Collection<? extends GrantedAuthority> authorityList = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		String havingAuthority = authorityList.stream().map(authority -> authority.getAuthority())
				.collect(Collectors.joining(","));
		return "Hello all " + havingAuthority;
	}
}
