package com.example.profile;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("default")
@Configuration
public class DefaultProfile {
	
	@PostConstruct
	public void postConstruct() {
		System.out.println("Default profile initialized.");
	}

}
