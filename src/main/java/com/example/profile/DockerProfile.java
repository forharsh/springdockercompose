package com.example.profile;

import javax.sql.DataSource;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Profile("docker")
@Configuration
public class DockerProfile {

	@Bean
	public WebServerFactoryCustomizer<TomcatServletWebServerFactory> sessionManagerCustomizer() {
		return server -> server.setPort(8086);
	}

	@Bean
	public DataSource dataSource() {

		final DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://mysql-standalone:3306/springsecurity");
		dataSource.setUsername("sa");
		dataSource.setPassword("password");

		return dataSource;
	}
}
