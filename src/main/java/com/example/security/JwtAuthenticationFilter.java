package com.example.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.exception.BadRequestException;
import com.example.model.Users;
import com.example.repository.UserRepository;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

	@Autowired
	private  JwtTokenProvider tokenProvider;

	@Autowired
	private  UserRepository userRepository;

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
			throws ServletException, IOException {
		try {
			 final String jwt = getJwtFromRequest(request);

			 if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
				tokenProvider.validateToken(jwt);
				final String user = tokenProvider.getUserIdFromJWT(jwt);
				final Users users = userRepository.findByUsername(user).orElseThrow(() -> new Exception("User not found"));

				final List<GrantedAuthority> authorities = users.getRoles().stream()
						.map(role -> new SimpleGrantedAuthority(role.getAuthority())).collect(Collectors.toList());

				final UsernamePasswordAuthenticationToken authenticationToken = user != null
						? new UsernamePasswordAuthenticationToken(user, null, authorities)
						: null;

				authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(authenticationToken);
			}
		} catch (final Exception ex) {
			logger.error("Could not set user authentication in security context", ex);
			throw new BadRequestException(ex.getMessage());
		}

		filterChain.doFilter(request, response);
	}

	private String getJwtFromRequest(final HttpServletRequest request) {
		final String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

}
