package com.example.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class Users {
	
	private int enabled;
	@Id
//	@GeneratedValue(strategy=GenerationType.TABLE)
	private String username;
	private String password;
	@OneToMany(fetch = FetchType.EAGER, mappedBy= "username", cascade = CascadeType.ALL)
	//@JoinTable(joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
	private Set<Authorities> roles;
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(final int enabled) {
		this.enabled = enabled;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(final String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(final String password) {
		this.password = password;
	}
	public Set<Authorities> getRoles() {
		return roles;
	}
	public void setRoles(final Set<Authorities> roles) {
		this.roles = roles;
	}

}
