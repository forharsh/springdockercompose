package com.example.main;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//@EnableAutoConfiguration(exclude=org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class)
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.example.*")
@EnableJpaRepositories("com.example.*")
@EnableTransactionManagement
@EntityScan("com.example.*")
public class SpringSecurityApplication implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {
	
	//int i;

	public static void main(final String[] args) {
		final Runnable javarunner;
		new SpringApplicationBuilder().listeners(new SpringSecurityApplication())
				.sources(SpringSecurityApplication.class).run(args);
//		for (String name : applicationContext.getBeanDefinitionNames()) {
//			System.out.println(name);
//		}
	}

	@Override
	public void onApplicationEvent(final ApplicationEnvironmentPreparedEvent event) {
		final int i;
		
		if (event.getEnvironment().getActiveProfiles().length == 0) {
			event.getEnvironment().setActiveProfiles("local");
		}

	}
}
