package com.example.config;

import java.io.PrintWriter;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import com.example.security.JwtAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final BasicAuthenticationEntryPoint basicAuthenticationEntryPoint;
	private final DataSource dataSource;

	@Autowired
	public SecurityConfig(final BasicAuthenticationEntryPoint basicAuthenticationEntryPoint, final DataSource dataSource) {
		this.basicAuthenticationEntryPoint = basicAuthenticationEntryPoint;
		this.dataSource = dataSource;
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers("/admin/private").hasRole("ADMIN")
				.antMatchers("/admin/add").permitAll().antMatchers("/user/public").hasRole("USER")
				.antMatchers("/all").permitAll().and().exceptionHandling()
				// Access denied handler get called when user has logged but not authorize.
				.accessDeniedHandler((request, response, accessDeniedException) -> {
					System.out.println("This handler get called when user is logged in");
					final PrintWriter writer = response.getWriter();
					writer.println("Access Denied !!");
				}).and().httpBasic() // HttpBasic used for Basic Authentication.
				.authenticationEntryPoint(basicAuthenticationEntryPoint).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		// Add our custom JWT security filter
		http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(dataSource);
	}


	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
}
