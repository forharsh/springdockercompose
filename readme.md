docker images
docker pull <image name>
docker build -f Dockerfile -t <image name> <. or directory path>
docker run -p 8085:8085 <image name>

=================================================
Connect with mysql

docker pull mysql:5.6
docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=springsecurity -e MYSQL_USER=sa -e MYSQL_PASSWORD=password -d mysql:5.6
docker container ls
docker images ls

link spring boot application to mysql container

docker run --name spring-boot-container -p 8085:8085 --link mysql-standalone:mysql  docker-spring-security1

=================================================

Interact with mysql to query db

docker exec -it mysql-standalone mysql -uroot -p