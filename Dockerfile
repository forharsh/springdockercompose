FROM openjdk:8
ADD target/SpringSecurityApplication-0.0.1-SNAPSHOT.jar SpringSecurityApplication-app.jar
EXPOSE 8085
ENTRYPOINT ["java","-jar","-Dspring.profiles.active=docker","SpringSecurityApplication-app.jar"]
CMD ["echo" , "Image Created"] 
